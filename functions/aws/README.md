# AWS functions

## Steps (within the browser)

* Login to your aws IAM account [here](https://console.aws.amazon.com/iam)

* Create a user with **Programmatic access** enabled
* Get the **Access Key ID** and **Secret access key** before exiting the workflow

## Steps (within the Docker container)

### Setup
* Login if credentials not recognized:

    > $ aws configure

* Create a S3 bucket:

    > $ aws s3api create-bucket --bucket lambda-bucket-epic --region us-west-2 --create-bucket-configuration LocationConstraint=us-west-2

### Implement and deploy

Code obtained from [here](https://itnext.io/creating-aws-lambda-applications-with-sam-dd13258c16dd)

* Navigate to the workshop's aws folder:

    > $ cd /home/serverless/workshop/functions/aws/sam-hello-world

* Package the application:

    > $ sam package \
    --template-file template.yml \
    --output-template-file package.yml \
    --s3-bucket my-bucket

* Then deploy it to AWS:

    > $ sam deploy \
    --template-file package.yml \
    --stack-name sam-hello-world-2 \
    --capabilities CAPABILITY_IAM

Going to https://console.aws.amazon.com/lambda/, you should see your new Lambda application **sam-hello-world-1** there.

Make sure you have the correct region selected.

Get the API endpoint and test it with CURL:

    > $ curl https://533lzekxvj.execute-api.us-west-2.amazonaws.com/Prod

Congrats!!