# GCE functions

## Steps (within the Docker container)

### Setup

* Login to your google account:

    > $ gcloud auth login

    This will output something like:

    > $ Go to the following link in your browser:
        https://accounts.google.com/o/oauth2/auth?... https%3A%2F%2Fwww.googleapis.com%2Fauth%2Faccounts.reauth
    > Enter verification code: 

    Simply follow the instructions to get the credentials

* Create a project and make it current (with unique ID):

    > $ gcloud projects create serverless-workshop-{xxx}
    > $ gcloud config set project serverless-workshop-{xxx}

* Create a Service Account for the workshop and assing roles to it:

    > $ gcloud iam service-accounts create serverless     --description "Serverless workshop Service Account"     --display-name "serverless"


    > $ gcloud projects add-iam-policy-binding serverless-workshop-01 --member serviceAccount:serverless@serverless-workshop-{xxx}.iam.gserviceaccount.com --role roles/cloudfunctions.admin

* Get the credentials:

    > $ gcloud iam service-accounts keys create ~/key.json --iam-account serverless@serverless-workshop-{xxx}.iam.gserviceaccount.com 

### Implement and deploy

* Navigate to the workshop's gce folder:

    > $ cd /home/serverless/workshop/functions/gce/gce-hello-world

* Deploy:

    > $ gcloud functions deploy hello_http --runtime python37 --trigger-http

Use the browser to access https://console.cloud.google.com/functions, your functions should be there.

* Test it:

    > $ curl https://us-central1-serverless-workshop-01.cloudfunctions.net/hello_http
