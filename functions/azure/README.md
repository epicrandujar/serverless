# Azure functions

## Steps (within the Docker container)

### Login

* Login to your azure account:

    > $ azure login

### Setup

* Create a resource group. An Azure resource group is a logical container into which Azure resources like function apps, databases, and storage accounts are deployed and managed:

    > $ azure group create --name serverlessGroup --location westeurope

* When prompted, choose the option that you prefer:

    ```
    Select y to enable data collection :(y/n) n
    ```
* Choose the name of your function's bucket. Storage account name must be between 3 and 24 characters in length and use numbers and lower-case letters only.
You can verify if the <storage_name> of your choice is valid by executing the command:

    > $ azure storage account check <storage_name>
    ```
    info:    Executing command storage account check
    Checking availability of the storage account name                            
    data:    Availability: false
    data:    Reason: AccountNameInvalid
    data:    Message: serverlessworkshopbucket01 is not a valid storage account name. Storage account name must be between 3 and 24 characters in length and use numbers and lower-case letters only.
    info:    storage account check command OK
    ```

* Create an Azure Storage account. Functions uses a general-purpose account in Azure Storage to maintain state and other information about your functions:

    > $ azure storage account create <storage_name> --location westeurope --resource-group serverlessGroup

* When prompted, select LRS and Storage

    ```
    info:    Executing command storage account create
    + Checking availability of the storage account name                            
    help:    SKU Name: 
    1) LRS
    2) ZRS
    3) GRS
    4) RAGRS
    5) PLRS
    : 1
    help:    Kind: 
    1) Storage
    2) BlobStorage
    : 1
    + Creating storage account                                                     
    info:    storage account create command OK
    ```
    
### Implement and deploy

* Navigate to the workshop's azure folder:

    > $ cd /home/serverless/workshop/functions/azure

* Create a function app project in the **azure-hello-world** folder of the current local directory. A GitHub repo is also created in **azure-hello-world**.

    > $ func init azure-hello-world

* When prompted, choose the runtime of preference:
    ```
    Select a worker runtime:
    1. dotnet
    2. node
    3. python
    4. powershell (preview)
    ```

* Navigate to the created folder:

    > $ cd azure-hello-world

* To enable extension bundles, open the **host.json** file and update its contents to match the following code:

        {
         "version": "2.0",
         "extensionBundle": {
             "id": "Microsoft.Azure.Functions.ExtensionBundle",
             "version": "[1.*, 2.0.0)"
         }
    }

* Create a function

    > $ func new --name helloHttpTrigger --template "HttpTrigger"

* Run the function locally

    > $ func start --build

* Test the function locally:
    
    > $ curl http://localhost:7071/api/helloHttpTrigger?name=serverless

* DEPLOYMENT:

    > $ az functionapp create --resource-group myResourceGroup --consumption-plan-location westeurope \
--name <APP_NAME> --storage-account  <STORAGE_NAME> --runtime <language>