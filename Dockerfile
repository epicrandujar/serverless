# Get source image from ubuntu
FROM python:3.6
RUN apt-get update

# Install wget to collect packages needed
RUN apt-get install wget -y

# Retrieve code-server and untar it
# code-server is VS Code running on a remote server,
# accessible through the browser.
RUN wget https://github.com/cdr/code-server/releases/download/2.1523-vsc1.38.1/code-server2.1523-vsc1.38.1-linux-x86_64.tar.gz
RUN tar -C /home -zxvf code-server2.1523-vsc1.38.1-linux-x86_64.tar.gz 
RUN mv /home/code-server2.1523-vsc1.38.1-linux-x86_64 /home/code-server
RUN rm -r code-server2.1523-vsc1.38.1-linux-x86_64.tar.gz

# Get npm and set node to version 8
RUN apt-get update
RUN apt-get install npm -y
RUN npm install -g n
RUN n 8
# Get pip3
RUN apt-get install python3-pip -y

######################################
### GCE
######################################
# Get Google Cloud
# Add the Cloud SDK distribution URI as a package source
RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list

# Import the Google Cloud Platform public key
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
# Update the package list and install the Cloud SDK
RUN apt-get update
RUN apt-get install google-cloud-sdk -y
# Get the GCE cloud functions emulator
# GCE's official emulator (only Node.js)
RUN npm install @google-cloud/functions-framework
# GCPFEMU (https://gitlab.com/divisadero/cloud-functions-python-emulator)
RUN pip install gcp-functions-emulator

######################################
### Azure
######################################
# Get Azure CLI
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

ENV AZURE_CLI_VERSION "0.10.13"

RUN apt-get update -qq && \
    apt-get install -qqy --no-install-recommends\
      apt-transport-https \
      build-essential \
      curl \
      ca-certificates \
      lsb-release \
      python-all \
      rlwrap \
      vim \
      nano \
      jq && \
    rm -rf /var/lib/apt/lists/* && \
    npm install --global azure-cli@${AZURE_CLI_VERSION} && \
    azure --completion >> ~/azure.completion.sh && \
    echo 'source ~/azure.completion.sh' >> ~/.bashrc && \
    azure

# Get azure functions needed packages
RUN wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.asc.gpg
RUN mv microsoft.asc.gpg /etc/apt/trusted.gpg.d/
RUN wget -q https://packages.microsoft.com/config/debian/9/prod.list
RUN mv prod.list /etc/apt/sources.list.d/microsoft-prod.list
RUN chown root:root /etc/apt/trusted.gpg.d/microsoft.asc.gpg
RUN chown root:root /etc/apt/sources.list.d/microsoft-prod.list
RUN apt-get update
RUN apt-get install azure-functions-core-tools -y

######################################
### AWS
######################################

# Get AWS CLI
RUN pip3 install awscli --upgrade
# Get AWS SAM CLI
RUN pip3 install aws-sam-cli
# Prepare a folder to save extension settings
#RUN mkdir -p /home/serverless/.local/share/code-server/User/globalStorage
# Install Docker to be able to test lambda functions locally
RUN apt-get install docker.io -y
######################################
### Image setup
######################################

# Share the port for exposing code-server
EXPOSE 8080
EXPOSE 7071
RUN useradd -ms /bin/bash serverless
USER serverless
# Retrieve compatible azure extensions to later install them
# in the IDE
COPY code-server/extensions /home/serverless/extensions

# Execute code-server
ENTRYPOINT ["./home/code-server/code-server"]
