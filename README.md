# serverless

Development environment for serverless workshop.

# Prerrequisites

You will need a functional account in:
- AWS
- Azure
- Google Cloud Platform

In your system, a running installation of Docker:


# Steps

Build the docker image  in the repository to launch VS Code in the browser.

> $ docker build –t serverless .​

> $  docker run --network host -v $(d):/home/serverless/workshop --name serverless-container -it -v ~/.aws:/home/serverless/.aws -v /var/run/docker.sock:/var/run/docker.sock serverless


A server will be made available in http://localhost:8080
